class ShopifyRequestsWarning(Warning):
    """Base warning for ShopifyRequests."""


class ResourceDeprecationWarning(ShopifyRequestsWarning, DeprecationWarning):
    """Shopify has deprecated the resource or property."""
