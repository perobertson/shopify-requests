import json
import warnings
from base64 import urlsafe_b64encode
from time import time

import pytest
import requests

from shopify_requests import RestClient


def bytes_to_str(b):
    from builtins import bytes, chr
    if b is None:
        return None
    result = str('')
    for myint in bytes(b):
        result += chr(myint)
    return result


def auth_header(user, password):
    to_encode = user + b':' + password
    return bytes_to_str(b'Basic ' + urlsafe_b64encode(to_encode))


def test_rest_client_requires_access_token():
    with pytest.raises(ValueError, match=r'access_token.*required'):
        RestClient('foo.myshopify.com')


def test_rest_client_requires_username_and_password():
    with pytest.raises(ValueError, match=r'password.*required'):
        RestClient('foo.myshopify.com', username='user')

    with pytest.raises(ValueError, match=r'username.*required'):
        RestClient('foo.myshopify.com', password='password')


def assert_request_headers(request, expected_headers=None):
    assert 'application/json' == request.headers['Accept']

    if expected_headers is None:
        expected_headers = []
    for key, val in expected_headers:
        assert val == request.headers[key]


def test_retry_after_rate_limit_waits_between_attempts(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', [
        {'status_code': 429},
        {'status_code': 200, 'json': response_content},
    ])

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    start = time()
    response = client.get('shop.json')
    end = time()
    assert 200 == response.status_code
    assert response_content == response.json()
    assert 0.5 <= end - start, 'default settings should have waited at least half a second between attempts'
    assert 2 == g_shop.call_count


def test_limit_backoff_factor(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', [
        {'status_code': 429},
        {'status_code': 200, 'json': response_content},
    ])

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1, limit_backoff_factor=0.1)
    start = time()
    response = client.get('shop.json')
    end = time()
    assert 200 == response.status_code
    assert response_content == response.json()
    assert 0.1 <= end - start, 'limit backoff factor is not waiting long enough'
    assert 2 == g_shop.call_count


def test_max_limit_retries_returns_final_status(requests_mock):
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', status_code=429)

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    start = time()
    response = client.get('shop.json')
    end = time()
    assert 429 == response.status_code
    assert 0.5 <= end - start, 'default settings should have waited at least half a second between attempts'
    assert 2 == g_shop.call_count


def test_max_limit_retries_defaults_to_0(requests_mock):
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', status_code=429)
    client = RestClient('foo.myshopify.com', access_token='token')
    response = client.get('shop.json')
    assert 429 == response.status_code
    assert 1 == g_shop.call_count


def test_get(requests_mock):
    def do_asserts(client, expected_headers):
        response_content = {'shop': {'id': 1}}
        g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', json=response_content)
        response = client.get('shop.json')
        assert 1 == g_shop.call_count
        assert isinstance(response, requests.Response)
        assert response_content == response.json()
        assert_request_headers(response.request, expected_headers)

    client = RestClient('foo.myshopify.com', access_token='token')
    do_asserts(client, [('X-Shopify-Access-Token', 'token')])

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    do_asserts(client, [('Authorization', auth_header(b'user', b'pass'))])


def test_get_with_additional_args_are_passed_to_requests(requests_mock):
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json')

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    response = client.get('shop.json', headers={'additional': 'content'})
    assert 1 == g_shop.call_count
    assert 'content' == response.request.headers['additional']


def test_post(requests_mock):
    def do_asserts(client, expected_headers):
        response_content = {'customer': {'id': 1, 'email': 'foo@bar.com'}}
        c_customer = requests_mock.post('https://foo.myshopify.com/admin/api/customers.json', json=response_content)
        request_json = {'customer': {'email': 'foo@bar.com'}}
        response = client.post('customers.json', json=request_json)
        assert 1 == c_customer.call_count
        assert isinstance(response, requests.Response)
        assert response_content == response.json()
        request = response.request  # type: requests.models.PreparedRequest
        assert request_json == json.loads(bytes_to_str(request.body))
        assert_request_headers(request, expected_headers)

    client = RestClient('foo.myshopify.com', access_token='token')
    do_asserts(client, [('X-Shopify-Access-Token', 'token')])

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    do_asserts(client, [('Authorization', auth_header(b'user', b'pass'))])


def test_post_with_additional_args_are_passed_to_requests(requests_mock):
    c_customer = requests_mock.post('https://foo.myshopify.com/admin/api/customers.json')

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    response = client.post('customers.json', headers={'additional': 'content'})
    assert 1 == c_customer.call_count
    assert 'content' == response.request.headers['additional']


def test_patch(requests_mock):
    def do_asserts(client, expected_headers):
        response_content = {'customer': {'id': 1, 'email': 'foo@bar.com'}}
        u_customer = requests_mock.patch('https://foo.myshopify.com/admin/api/customers/1.json', json=response_content)
        request_json = {'customer': {'email': 'foo@bar.com'}}
        response = client.patch('customers/1.json', json=request_json)
        assert 1 == u_customer.call_count
        assert isinstance(response, requests.Response)
        assert response_content == response.json()
        request = response.request  # type: requests.models.PreparedRequest
        assert request_json == json.loads(bytes_to_str(request.body))
        assert_request_headers(request, expected_headers)

    client = RestClient('foo.myshopify.com', access_token='token')
    do_asserts(client, [('X-Shopify-Access-Token', 'token')])

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    do_asserts(client, [('Authorization', auth_header(b'user', b'pass'))])


def test_patch_with_additional_args_are_passed_to_requests(requests_mock):
    u_customer = requests_mock.patch('https://foo.myshopify.com/admin/api/customers/1.json')

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    response = client.patch('customers/1.json', headers={'additional': 'content'})
    assert 1 == u_customer.call_count
    assert 'content' == response.request.headers['additional']


def test_put(requests_mock):
    def do_asserts(client, expected_headers):
        response_content = {'customer': {'id': 1, 'email': 'foo@bar.com'}}
        u_shop = requests_mock.put('https://foo.myshopify.com/admin/api/customers/1.json', json=response_content)
        request_json = {'customer': {'email': 'foo@bar.com'}}
        response = client.put('customers/1.json', json=request_json)
        assert 1 == u_shop.call_count
        assert isinstance(response, requests.Response)
        assert response_content == response.json()
        request = response.request  # type: requests.models.PreparedRequest
        assert request_json == json.loads(bytes_to_str(request.body))
        assert_request_headers(request, expected_headers)

    client = RestClient('foo.myshopify.com', access_token='token')
    do_asserts(client, [('X-Shopify-Access-Token', 'token')])

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    do_asserts(client, [('Authorization', auth_header(b'user', b'pass'))])


def test_put_with_additional_args_are_passed_to_requests(requests_mock):
    u_customer = requests_mock.put('https://foo.myshopify.com/admin/api/customers/1.json')

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    response = client.put('customers/1.json', headers={'additional': 'content'})
    assert 1 == u_customer.call_count
    assert 'content' == response.request.headers['additional']


def test_delete(requests_mock):
    def do_asserts(client, expected_headers):
        u_shop = requests_mock.delete('https://foo.myshopify.com/admin/api/customers/1.json', status_code=204)
        request_json = {'customer': {'email': 'foo@bar.com'}}
        response = client.delete('customers/1.json', json=request_json)
        assert 1 == u_shop.call_count
        assert isinstance(response, requests.Response)
        assert '' == response.text
        request = response.request  # type: requests.models.PreparedRequest
        assert request_json == json.loads(bytes_to_str(request.body))
        assert_request_headers(request, expected_headers)

    client = RestClient('foo.myshopify.com', access_token='token')
    do_asserts(client, [('X-Shopify-Access-Token', 'token')])

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    do_asserts(client, [('Authorization', auth_header(b'user', b'pass'))])


def test_delete_with_additional_args_are_passed_to_requests(requests_mock):
    d_customer = requests_mock.delete('https://foo.myshopify.com/admin/api/customers/1.json')

    client = RestClient('foo.myshopify.com', username='user', password='pass')
    response = client.delete('customers/1.json', headers={'additional': 'content'})
    assert 1 == d_customer.call_count
    assert 'content' == response.request.headers['additional']


def test_calls_used(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get(
        'https://foo.myshopify.com/admin/api/shop.json',
        status_code=200,
        json=response_content,
        headers={'X-Shopify-Shop-Api-Call-Limit': '6/40'},
    )

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    client.get('shop.json')
    assert 1 == g_shop.call_count
    assert 6 == client.calls_used()


def test_calls_used_starts_at_0(requests_mock):
    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    assert 0 == client.calls_used()


def test_calls_used_after_server_error_remains_the_same(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get('https://foo.myshopify.com/admin/api/shop.json', [
        {'status_code': 200, 'json': response_content, 'headers': {'X-Shopify-Shop-Api-Call-Limit': '6/40'}},
        {'status_code': 500},
    ])

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    client.get('shop.json')
    assert 1 == g_shop.call_count
    assert 6 == client.calls_used()

    # Now check that a remote server error doesnt override the last request data
    client.get('shop.json')
    assert 2 == g_shop.call_count
    assert 6 == client.calls_used()


def test_calls_remaining(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get(
        'https://foo.myshopify.com/admin/api/shop.json',
        status_code=200,
        json=response_content,
        headers={'X-Shopify-Shop-Api-Call-Limit': '6/40'},
    )

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    client.get('shop.json')
    assert 1 == g_shop.call_count
    assert 34 == client.calls_remaining()


def test_max_available(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get(
        'https://foo.myshopify.com/admin/api/shop.json',
        status_code=200,
        json=response_content,
        headers={'X-Shopify-Shop-Api-Call-Limit': '6/40'},
    )

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    client.get('shop.json')
    assert 1 == g_shop.call_count
    assert 40 == client.max_available()


def test_api_version(requests_mock):
    version = '2019-04'
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get(
        "https://foo.myshopify.com/admin/api/{}/shop.json".format(version),
        status_code=200,
        json=response_content,
    )

    client = RestClient('foo.myshopify.com', access_token='token', version=version)
    client.get('shop.json')
    assert 1 == g_shop.call_count


def test_deprecated_resource_logs_warning(requests_mock):
    response_content = {'shop': {'id': 1}}
    g_shop = requests_mock.get(
        'https://foo.myshopify.com/admin/api/shop.json',
        status_code=200,
        json=response_content,
        headers={'X-Shopify-API-Deprecated-Reason': 'shop is being replaced with store'},
    )

    client = RestClient('foo.myshopify.com', access_token='token', max_limit_retries=1)
    with warnings.catch_warnings(record=True) as w:
        client.get('shop.json')
        assert len(w) == 1
        assert issubclass(w[-1].category, DeprecationWarning)
        assert 'shop is being replaced with store' == str(w[-1].message)
        assert 1 == g_shop.call_count

    # now without the deprecation header
    g_shop = requests_mock.get(
        'https://foo.myshopify.com/admin/api/shop.json',
        status_code=200,
        json=response_content,
    )
    with warnings.catch_warnings(record=True) as w:
        client.get('shop.json')
        assert len(w) == 0
        assert 1 == g_shop.call_count
