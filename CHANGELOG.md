# Change Log

## [Unreleased]

### Added

- Deprecation warning when Shopify returns the `X-Shopify-API-Deprecated-Reason` header - [#32](https://gitlab.com/perobertson/shopify-requests/merge_requests/32)

## [0.4.0] - 2019-04-13

### Added

- Added ability to switch API versions - [#24](https://gitlab.com/perobertson/shopify-requests/merge_requests/24) [#28](https://gitlab.com/perobertson/shopify-requests/merge_requests/28)
- Added api usage lookup - [#20](https://gitlab.com/perobertson/shopify-requests/merge_requests/20)
- Added [docs](https://shopify-requests.readthedocs.io/)

## [0.3.0] - 2019-03-23

### Added

- Addded ability to retry on rate limiting - [#17](https://gitlab.com/perobertson/shopify-requests/merge_requests/17)
- Added retries when data never made it to Shopify - [#14](https://gitlab.com/perobertson/shopify-requests/merge_requests/14)

## [0.2.0] - 2019-03-12

### Added

- Added support for python >=3.5.0, <4.0.0

## [0.1.1] - 2019-03-10

- Initial release

[Unreleased]: https://gitlab.com/perobertson/shopify-requests/compare/v0.4.0...master
[0.4.0]:  https://gitlab.com/perobertson/shopify-requests/tags/v0.4.0
[0.3.0]:  https://gitlab.com/perobertson/shopify-requests/tags/v0.3.0
[0.2.0]:  https://gitlab.com/perobertson/shopify-requests/tags/v0.2.0
[0.1.1]: https://gitlab.com/perobertson/shopify-requests/tags/v0.1.1

