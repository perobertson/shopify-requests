.. Shopify Requests documentation master file, created by
   sphinx-quickstart on Sun Mar 31 11:56:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shopify Requests' documentation!
============================================

The ShopifyRequests library is a wrapper around the python requests library.
Its main purpose is to remove the boiler plate code needed to do basic API
calls to Shopify.


User's Guide
============

The :class:`RestClient<shopify_requests.RestClient>` is meant to be as simple as using the requests library.
All the http verbs needed for REST calls are available.

* GET
* PUT
* POST
* PATCH
* DELETE

OAuth token example:

.. code-block:: python

    from shopify_requests import RestClient
    client = RestClient(
        'foo.myshopify.com',
        access_token='abc123',
    )
    response = client.get('shop.json')

Private app example:

.. code-block:: python

    from shopify_requests import RestClient
    client = RestClient(
        'foo.myshopify.com',
        username='1234',
        password='asdf',
    )
    response = client.get('shop.json')

By default the client also has safe retries enabled to help with network issues.
If a request fails to send data to Shopify, it will be retried a 3 times before
returning the failure response.
This can be configured with the `connect_retries` parameter.

.. code-block:: python

    from shopify_requests import RestClient
    client = RestClient(
        'foo.myshopify.com',
        access_token='abc123',
        connect_retries=5,
    )
    response = client.get('shop.json')

Reusing the client has the added benefit of reusing the http session once the
connection is established.
This means that subsequent calls will not have to do the SSL handshake.


API Docs
========

.. toctree::
   :maxdepth: 1
   :caption: API Docs:

   api/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
