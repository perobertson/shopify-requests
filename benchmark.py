import sys
from time import time

from requests import get

from shopify_requests import RestClient


def rest_client(domain, version, token, times):
    client = RestClient(domain, access_token=token, max_limit_retries=3, version=version)
    start = time()
    while times > 0:
        response = client.get('shop.json')
        response.raise_for_status()
        times -= 1
    end = time()
    print("{}s".format(end - start))


def requests(domain, version, token, times):
    url = "https://{}/admin/api/{}/shop.json".format(domain, version)
    headers = {'X-Shopify-Access-Token': token}
    start = time()
    while times > 0:
        get(url, headers=headers).raise_for_status()
        times -= 1
    end = time()
    print("{}s".format(end - start))


def main(argv):
    domain = argv[0]
    token = argv[1]
    times = int(argv[2])
    version = 'unstable'
    if len(argv) > 3:
        version = argv[3]
    rest_client(domain, version, token, times)
    requests(domain, version, token, times)


if __name__ == '__main__':
    main(sys.argv[1:])
